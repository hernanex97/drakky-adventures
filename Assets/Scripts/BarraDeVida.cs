﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour {

    PlayerController player;

    float maxHealth;

    float vidaActual;

    Image barraDeVida;
    public Text textoVida;

    void Start () 
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        barraDeVida = gameObject.GetComponent<Image>();       
        maxHealth = player.vidaPlayer;

	}
	
	void Update ()
    {

        vidaActual = player.vidaPlayer;

        barraDeVida.fillAmount = vidaActual / maxHealth;
        textoVida.text = vidaActual + " / " + maxHealth;
	}
}
