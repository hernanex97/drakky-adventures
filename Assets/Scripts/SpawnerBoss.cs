﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBoss : MonoBehaviour {

    public GameObject boss;
    public float rangoSpawner;
    public GameObject particulasCaida;

	void Start () 
    {
		
	}
	
	
	void Update () 
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Instantiate(boss, transform.position, transform.rotation);
            Instantiate(particulasCaida);
            Destroy(gameObject);
        }
    
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, rangoSpawner);
    }
}
