﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEnemigosContador : MonoBehaviour {

    public int EnemigosIniciales;
    public Text textoEnemigosContador;

	void Start () 
    {
		
	}

	void Update () 
    {
        EnemigosIniciales = GameManager.Instance.enemigosTotales;
        textoEnemigosContador.text = "Enemies Alive: " + EnemigosIniciales.ToString();



	}
}
