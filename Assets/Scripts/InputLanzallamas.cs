﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputLanzallamas : MonoBehaviour {

    public GameObject lanzallamas;
    


    void Update()
    {
        if(GameManager.Instance.enemigosTotales <= 5 || GameManager.Instance.pasadoNivelUno == true)
        {
            Lanzallamas();
        }
             
    }

    void Lanzallamas()
    {
        if(Input.GetKeyDown(KeyCode.R))
            Instantiate(lanzallamas, transform.position, transform.rotation);
                     
    }
	
}
