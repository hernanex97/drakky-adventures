﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyCrabController : MonoBehaviour {

    public float vidaCrab = 100f;
    public float damage = 10f;   
    public float radioVision;
    public float velocidad = 4f;
    public float rangoRecibirDaño;

    public Transform recibirDañoTransform;
    
    private Animator anim;
    GameObject player;
    
   //Posicion inicial 
    public Vector3 posicionInicial;

    public Transform[] posPatrulla;
    public float radioAtaque;
    public bool patruyando;
    public bool siguiendo;
    private int index;
    

    NavMeshAgent agent;
    PlayerController playerScript;
    CrabPinzaScript pinzaScript;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Start () 
    {
        //encuentra al player
        player = GameObject.FindGameObjectWithTag("Player");
        posicionInicial = transform.position;
        pinzaScript = GetComponentInChildren<CrabPinzaScript>();

        agent = GetComponent<NavMeshAgent>();
        playerScript = FindObjectOfType<PlayerController>();
        agent.speed = velocidad;
	}
	
	
	void Update () 
    {
        if (player == null) return;

        Vector3 target = posicionInicial;
        float fixedVelocidad = velocidad * Time.deltaTime;
        float distancia = Vector3.Distance(player.transform.position, transform.position);

        if (distancia < radioVision)
        {
            patruyando = false;
            siguiendo = true;
            
        }
        else
        {
            patruyando = true;
            siguiendo = false;
            anim.SetBool("AtaqueCrab", false);
        }

       
        if (patruyando && posPatrulla.Length > 0)
        {
            
            float distPuntos = Vector3.Distance(transform.position, posPatrulla[index].position);
            
            agent.stoppingDistance = 1f;
            if(distPuntos > 1)
            {
                agent.destination = posPatrulla[index].position;
                pinzaScript.coolDownTimer = 0f;                
            }
            else if (index < posPatrulla.Length -1)
            {
                index++;
            }
            else
            {
                index = 0;
            }

        }
        
       if(siguiendo == true)
       {
           if(distancia < radioVision)
           {    
                agent.destination = player.transform.position;
                //agent.transform.LookAt(player.transform.position);
                //agent.stoppingDistance = 4.5f;                
                anim.SetBool("AtaqueCrab", true);
           }
           
       }     

      //Muere Crab
      if (vidaCrab <= 0)
      {
          Destroy(gameObject);
          GameManager.Instance.enemigosTotales -= 1;
      }

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radioVision);

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(recibirDañoTransform.transform.position, rangoRecibirDaño);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radioAtaque);
    }
    
    
    
}
