﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapidaScript : MonoBehaviour {

    PlayerController playerScript;
    public GameObject momia;
    public GameObject spawn;
    public GameObject particulasMomia;

    public AudioClip spawnSound;
    public AudioSource audioSource;

    public float radioLapida;
    bool existeMomia = false;

    void Start () 
    {
        //player = GameObject.FindGameObjectWithTag("Player");
        playerScript = FindObjectOfType<PlayerController>();
	}
	
	
	void Update () 
    {
        float distanciaAPlayer = Vector3.Distance(playerScript.transform.position, transform.position);

		if(distanciaAPlayer < radioLapida)
        {
            InstanciarMomia();
        }
        
	}

    void InstanciarMomia()
    {

        if (existeMomia == false)
        {   
            Instantiate(momia, spawn.transform.position, spawn.transform.rotation);
            SonidoSpawnMomia();
            Instantiate(particulasMomia, spawn.transform.position, spawn.transform.rotation);
            existeMomia = true;
        }
    }
    
    void SonidoSpawnMomia()
    {
        audioSource.PlayOneShot(spawnSound);
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radioLapida);
        
    }

}
