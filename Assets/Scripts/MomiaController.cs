﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MomiaController : MonoBehaviour {

    PlayerController playerScript;
    NavMeshAgent agent;
    public GameObject Talisman;
    public Transform Disparador;
    public AudioClip momiaClip;
    public AudioClip momiaAtaque;
    public AudioSource audioSource;
    public float vidaMomia;
    public float radioAtaque;
    public float velocidad;

    //CoolDown
    public float coolDownTimer;
    public float coolDown = 1f;

	void Start () 
    {
        playerScript = FindObjectOfType<PlayerController>();
        agent = GetComponent<NavMeshAgent>();
        agent.speed = velocidad;
	}
	
	
	void FixedUpdate () 
    {
        
        float distanciaAlPlayer = Vector3.Distance(playerScript.transform.position, transform.position);
        CoolDown();

        if (distanciaAlPlayer < radioAtaque)
        {
            agent.destination = playerScript.transform.position;
            if (coolDownTimer == 0)
            {
                StartCoroutine(AtaqueTalisman());
                coolDownTimer = coolDown;
                
            }
        }
        else
        {
            StopAllCoroutines();
        }

        if (vidaMomia <= 0)
        {
            Destroy(gameObject);
            GameManager.Instance.enemigosTotales -= 1;
        }



    }



    IEnumerator AtaqueTalisman()
    {
            Instantiate(Talisman, Disparador.transform.position, Disparador.transform.rotation);
            audioSource.PlayOneShot(momiaAtaque);
            Talisman.transform.Rotate(-90f, 0f, 0f);
            yield return new WaitForSeconds(1.5f);
            
    }

    

    void CoolDown()
    {
        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

        if (coolDownTimer < 0)
        {
            coolDownTimer = 0;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radioAtaque);
    }
}
