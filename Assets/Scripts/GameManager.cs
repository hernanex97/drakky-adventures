﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;
    public int enemigosTotales;
    public int enemigosTotalesFinal;

    public GameObject huevoN1;
    public GameObject huevoN2;
    public GameObject huevoN3;
    public GameObject pauseMenu;
    
    public Transform posHuevo;
    public Transform posHuevo2;
    public Transform posHuevo3;

    public bool crearHuevoUno = true;
    public bool crearHuevoDos = true;
    public bool crearHuevoTres = true;

    public bool pasadoNivelUno = false;
    public bool pasadoNivelDos = false;
    public bool pasadoNivelTres = false;

    private void Awake()
    {
        //Si solo hay un GM, no te destruyas.
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        //Si hay otro GM, destruite.
        else
        {
            Destroy(gameObject);
        }

        enemigosTotales = 10;
        enemigosTotalesFinal = 1;
        
        
    }

    private void Start()
    {
        enemigosTotales = 10;
        
        //pasadoNivelDos = false;   
        //pasadoNivelUno = false;
    }

    void Update()
    {
        //Para Level Uno
        if (enemigosTotales == 0 && crearHuevoUno == true && pasadoNivelUno == false)
        {
            InstanciarHuevo1();
            crearHuevoUno = false;
            pasadoNivelUno = true;
        }
        
        if(pasadoNivelUno == true)
        {
            pasadoNivelDos = false;
            crearHuevoDos = true;
            pasadoNivelUno = false;
        }


        //Para Level Dos
        if (enemigosTotales == 0 && crearHuevoDos == true && pasadoNivelDos == false)
        {
            print("posHuevo");
            InstanciarHuevo2();
            crearHuevoDos = false;
            pasadoNivelDos = true;
        }

        if (pasadoNivelDos == true)
        {
            pasadoNivelTres = false;
            crearHuevoTres = true;
            pasadoNivelDos = false;         
        }

        //Para Level Tres
        if (enemigosTotalesFinal == 0 && crearHuevoTres == true && pasadoNivelTres == false)
        {
            InstanciarHuevo3();
            crearHuevoTres = false;
            pasadoNivelTres = true;
        }
    }

    void InstanciarHuevo1()
    {
        Instantiate(huevoN1, posHuevo.transform.position, posHuevo.transform.rotation);
    }

    void InstanciarHuevo2()
    {
        print("Instanciando heuvo");
        Instantiate(huevoN2, posHuevo2.transform.position, posHuevo2.transform.rotation);
    }

    void InstanciarHuevo3()
    {
        Instantiate(huevoN3, posHuevo3.transform.position, posHuevo3.transform.rotation);
    }

    public void Ganaste1()
    {        
        SceneManager.LoadScene("Ganaste1");
        enemigosTotales = 10;


    }

    public void Ganaste2()
    {
        SceneManager.LoadScene("Ganaste2");
        
    }
    
    public void Ganaste3()
    {
        SceneManager.LoadScene("Ganaste3");
        
    }

    public void LevelTwo()
    {
        SceneManager.LoadScene("LevelTwo");
        enemigosTotales = 10;
              
    
    }

    //public void LevelThree()
    //{
    //    SceneManager.LoadScene("LevelThreeFinal");
    //    enemigosTotales = 1;
    //    pasadoNivelTres = false;
    //}

}
