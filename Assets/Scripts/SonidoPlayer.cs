﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoPlayer : MonoBehaviour {

    public AudioClip swordClip;
    public AudioClip jumpPlayer;

    

    public AudioSource audioSource;

    void Start()
    {
        
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    

    public void ReproducirSonido()
    {
        audioSource.PlayOneShot(swordClip);
    }
}
