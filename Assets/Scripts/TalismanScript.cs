﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalismanScript : MonoBehaviour {

    
    PlayerController playerScript;
    public float velocidad;
    public float damage;


	void Start () 
    {
        playerScript = FindObjectOfType<PlayerController>();
	}
	
	
	void FixedUpdate () 
    {
        gameObject.transform.Translate(0f, 0f, velocidad * Time.deltaTime);
        Destroy(gameObject, 3f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            playerScript.vidaPlayer -= damage;
            Destroy(gameObject);
        }
        
    }








}
