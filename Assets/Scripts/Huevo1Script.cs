﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Huevo1Script : MonoBehaviour {

	
    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Player")
        {
            
            GameManager.Instance.Ganaste1();
        }
    }

}
