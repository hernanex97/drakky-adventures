﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ResetearLevel : MonoBehaviour
{
    public void RestartScene(string nameScene)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


}
