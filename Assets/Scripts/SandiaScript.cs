﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandiaScript : MonoBehaviour {

    PlayerController player;
    Animator animator;
    
    public float aumentoVida;
    public AudioClip audioComer;
    public AudioSource audioSource;
    

    void Start ()   
    {
        player = FindObjectOfType<PlayerController>();
        animator = GetComponent<Animator>();
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void Update()
    {
        animator.SetBool("Giro", true);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            player.vidaPlayer += aumentoVida;
            audioSource.PlayOneShot(audioComer);
            Destroy(gameObject, 0.6f);           
        }
    }







}
