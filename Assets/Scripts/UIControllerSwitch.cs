﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIControllerSwitch : MonoBehaviour {

    public UiEnum ActivarUI;

    public GameObject UIControles;
    public GameObject UIHistoria;

    public enum UiEnum
    {
        Controles,
        Historia
    }

    private void Start()
    {
        Time.timeScale = 0f;
    }


    void Update()
    {
        SwitchUI();
    }

    void SwitchUI()
    {
        switch(ActivarUI)
        {
            case UiEnum.Controles:
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    UIHistoria.SetActive(false);
                    UIControles.SetActive(true);
                    ActivarUI = UiEnum.Historia;
                }
                break;


            case UiEnum.Historia:                
                if(Input.GetKeyDown(KeyCode.Return))
                {
                    UIControles.SetActive(false);
                    Time.timeScale = 1f;
                }
                break;
        }
    }
}
