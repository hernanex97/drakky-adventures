﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVidaBoss : MonoBehaviour {

    BossController bossScript;
    
    float maxHealth;
    float vidaActual;
    Image barraDeVida;
    public Text textoVida;

    void Start () 
    {
        bossScript = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossController>();
        maxHealth = bossScript.vidaBoss;
        barraDeVida = gameObject.GetComponent<Image>();
        
        
    }
	
	
	void Update () 
    {
        vidaActual = bossScript.vidaBoss;
        barraDeVida.fillAmount = vidaActual / maxHealth;
        textoVida.text = vidaActual + " / " + maxHealth;
    }
}
