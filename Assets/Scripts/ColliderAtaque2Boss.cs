﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderAtaque2Boss : MonoBehaviour {

    PlayerController playerScript;
    BossController bossScript;
    public float damageCollider;

	void Start () 
    {
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        bossScript = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossController>();
        
    }
	
	
	void Update () 
    {
        Destroy(gameObject, 0.5f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerScript.vidaPlayer -= damageCollider;
            Destroy(gameObject);
        }
        
    }


}
