﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    
    public float velocidad;
    public float salto;
    public float velShift;

    private Rigidbody rb;
    private Vector3 playerPos;
    private Animator anim;

    public float vidaPlayer = 100f;
    public float damageHachas;
    public float damageLanzallamas;
    
    public LayerMask layerPiso;
    public Transform detectorPiso;
    public float radioDetector;
    public bool tocandoPiso;

    public float radioAtaque = 3f;
    public Transform ataqueRadioTransform;

    private bool puedeLastimar = false;
    private bool prohibidoLastimar;

    public float anguloVision;

    public float coolDown = 1f;
    public float coolDownTimer;

    public GameObject sangrePlayer;
    
    public EnemyCrabController enemigo;
    public MomiaController momiaScript;
    public BossController bossScript;

    SonidoPlayer playerSonido;

    void Awake()
    {        
        anim = GetComponent<Animator>();
    }



    void Start () 
    {
        rb = GetComponent<Rigidbody>();
        //Primero accedo al primer objeto con el componente y despues al componente directamente y lo guardo en enemigo.
        //enemigo = FindObjectOfType<EnemyCrabController>().GetComponent<EnemyCrabController>();
        playerSonido = GetComponent<SonidoPlayer>();
        
    }
	
    
	
	void FixedUpdate () 
    {
        
        tocandoPiso = Physics.CheckSphere(detectorPiso.position, radioDetector, layerPiso);
        playerPos = Vector3.zero;

        Movimiento();
        CoolDown();
        


        //Ataque Input
        if (Input.GetKey(KeyCode.Mouse0) && coolDownTimer == 0)
        {
            anim.SetBool("Atacar1", true);
            //VisionPlayer();
            HerirBicho();
            coolDownTimer = coolDown;
        }
        else if(Input.GetKeyUp(KeyCode.Mouse0))
        {
            anim.SetBool("Atacar1", false);
        }

        //Vida Player

        vidaPlayer = Mathf.Clamp(vidaPlayer, 0, 100);
        if(vidaPlayer <= 0)
        {
            Debug.Log("Me Morí.");
            anim.SetBool("Dead", true);
            Destroy(gameObject, 3f);
            SceneManager.LoadScene("Moriste");
            
        }

    }


    //Gizmos
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(detectorPiso.position, radioDetector);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(ataqueRadioTransform.transform.position, radioAtaque);

        //vision gizmos.
        Gizmos.color = Color.blue;
        float mitadAngulo = anguloVision / 2.0f;

        Quaternion rotRayoIzq = Quaternion.AngleAxis(-mitadAngulo, Vector3.up);
        Quaternion rotRayoDer = Quaternion.AngleAxis(mitadAngulo, Vector3.up);

        Vector3 dirRayoIzq = rotRayoIzq * transform.forward;
        Vector3 dirRayoDer = rotRayoDer * transform.forward;

        Gizmos.DrawRay(transform.position, dirRayoIzq * anguloVision);
        Gizmos.DrawRay(transform.position, dirRayoDer * anguloVision);




    }

    //Hacer daño a enemigos.
    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.tag == "Enemigo")
        {

            puedeLastimar = true;
            enemigo = other.gameObject.GetComponent<EnemyCrabController>();
        }
        else if (other.gameObject.tag == "Momia")
        {

            puedeLastimar = true;
            momiaScript = other.gameObject.GetComponent<MomiaController>();
            
        }
        else if (other.gameObject.tag == "Boss")
        {
            bossScript = other.gameObject.GetComponent<BossController>();
            puedeLastimar = true;
            print("tocando a boss");
            
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        puedeLastimar = false;
    }


    //Caer a precipicio
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Precipicio")
        {
            anim.SetBool("Dead", true);
            Destroy(gameObject, 3f);
            SceneManager.LoadScene("Moriste");
        }
    }

    //daño a crab
    private void VisionPlayer(EnemyCrabController enemigo)
    {
       
       //Direccion del Player
       Vector3 direccionPlayer = enemigo.transform.position - transform.position;

       float angulo = Vector3.Angle(direccionPlayer, transform.forward);
       

       if (enemigo != null && angulo < anguloVision / 2f)
       {
           puedeLastimar = true;
       }
       else puedeLastimar = false;
    }

    //daño a momia
    private void VisionPlayer(MomiaController momiaScript)
    {

        //Direccion del Player
        Vector3 direccionPlayer = momiaScript.transform.position - transform.position;

        float angulo = Vector3.Angle(direccionPlayer, transform.forward);

        
        if (momiaScript != null && angulo < anguloVision / 2f)
        {
            puedeLastimar = true;
            print(momiaScript);
        }
        else puedeLastimar = false;
    }

    //daño a BOSS
    private void VisionPlayer(BossController bossScript)
    {

        //Direccion del Player
        Vector3 direccionPlayer = bossScript.transform.position - transform.position;

        float angulo = Vector3.Angle(direccionPlayer, transform.forward);


        if (bossScript != null && angulo < anguloVision / 2f)
        {
            puedeLastimar = true;
        }
        else puedeLastimar = false;
    }

    void HerirBicho()
    {
        
        if (puedeLastimar == true)
        {                         
            if (enemigo != null)
            {

                enemigo.vidaCrab -= damageHachas;    
            }
            else if(momiaScript !=null)
            {               
                momiaScript.vidaMomia -= damageHachas;
            }
            else if (bossScript != null)
            {                
                bossScript.vidaBoss -= damageHachas;
            }



        }        
    }
    
    void Movimiento()
   {
        if (Input.GetKey(KeyCode.W))
        {
            anim.SetBool("IsWalk", true);
            transform.Translate(0f, 0f, velocidad * Time.fixedDeltaTime);
        }
        else
        {
            anim.SetBool("IsWalk", false);
        }


        if (Input.GetKey(KeyCode.A))
        {
            anim.SetBool("IsWalk", true);
            transform.Translate(-velocidad * Time.fixedDeltaTime, 0f, 0f);
        }


        if (Input.GetKey(KeyCode.S))
        {
            anim.SetBool("IsWalk", true);
            transform.Translate(0f, 0f, -velocidad * Time.fixedDeltaTime);
        }


        if (Input.GetKey(KeyCode.D))
        {
            anim.SetBool("IsWalk", true);
            transform.Translate(velocidad * Time.fixedDeltaTime, 0f, 0f);
        }


        if (Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetBool("IsRun", true);
            velocidad = 10f;
        }
        else
        {
            anim.SetBool("IsRun", false);
            velocidad = 5f;
        }

        if (Input.GetKey(KeyCode.Space) && tocandoPiso)
        {
            rb.AddForce(transform.up * salto, ForceMode.VelocityChange);
            playerSonido.audioSource.PlayOneShot(playerSonido.jumpPlayer);
            
        }

    }
    
    void CoolDown()
    {
        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

        if (coolDownTimer <= 0)
        {
            coolDownTimer = 0;
        }
    }

    public void InstanciarSangrePlayer()
    {
        Instantiate(sangrePlayer, transform.position, transform.rotation);
    }
}   
    