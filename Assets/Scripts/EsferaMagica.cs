﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsferaMagica : MonoBehaviour {

    PlayerController playerScript;
    public GameObject particulaGolpeEsfera;
    public float velocidad;
    public float damage;


    void Start()
    {
        playerScript = FindObjectOfType<PlayerController>();
    }


    void FixedUpdate()
    {
        gameObject.transform.Translate(0f, 0f, velocidad * Time.deltaTime);
        Destroy(gameObject, 3f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerScript.vidaPlayer -= damage;
            Instantiate(particulaGolpeEsfera, playerScript.transform.position, playerScript.transform.rotation);
            
            Destroy(gameObject);
        }

    }
}
