﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IrAEscena : MonoBehaviour {

   public void GoToEscena(string sceneName)
   {
       SceneManager.LoadScene(sceneName);
   }
    
   
}
