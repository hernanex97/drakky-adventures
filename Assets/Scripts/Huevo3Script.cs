﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Huevo3Script : MonoBehaviour {

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.Instance.Ganaste3();
        }
    }
}
