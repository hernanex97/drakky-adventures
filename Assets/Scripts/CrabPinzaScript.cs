﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabPinzaScript : MonoBehaviour {

    PlayerController player;
    

    public float coolDown = 0.1f;
    public float coolDownTimer;

    private void Update()
    {
        CoolDown();
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.tag == "Player" && coolDownTimer == 0f)
        {
            

            player = other.gameObject.GetComponent<PlayerController>();
            player.vidaPlayer -= 5f;
            player.InstanciarSangrePlayer();

        }
    }

    void CoolDown()
    {
        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

        if (coolDownTimer <= 0)
        {
            coolDownTimer = 0;
        }
    }


}
