﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossController : MonoBehaviour {

    public float vidaBoss;
    public float radioAtaque;
    public float rangoAtaque1;
    public float rangoAtaque2;
    public float velocidad;
    public float damage;

    public float coolDown = 2f;
    public float coolDownTimer;

    public GameObject esferaMagica;
    public GameObject capsula;

    public Transform spawnEsfera;
    public Transform transformCapsula;

    PlayerController playerScript;
    EnemyCrabController crabScript;
    MomiaController momiaScript;

    NavMeshAgent agent;
    Animator anim;

    public EstadosBoss estadoActual;
    public enum EstadosBoss
    {
        Estado1,
        Estado2,
        Estado3
    }

    void Start()
    {
        playerScript = FindObjectOfType<PlayerController>();
        crabScript = FindObjectOfType<EnemyCrabController>();
        momiaScript = FindObjectOfType<MomiaController>();
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        agent.speed = velocidad;
    }


    void Update()
    {

        CoolDown();
        float distAPlayer = Vector3.Distance(playerScript.transform.position, transform.position);
        if (distAPlayer < radioAtaque)
        {
            StartCoroutine(CorrutinasDelBoss());
        }
       
        if(vidaBoss <= 0)
        {
            Destroy(gameObject);
            GameManager.Instance.enemigosTotalesFinal -= 1;
        }


    }

    IEnumerator CorrutinasDelBoss()
    {
        while (true)
        {
            switch (estadoActual)
            {
                case EstadosBoss.Estado1:
                    yield return StartCoroutine(EstadoBoss1());
                    break;

                case EstadosBoss.Estado2:                    
                    yield return StartCoroutine(EstadoBoss2());
                    break;

                case EstadosBoss.Estado3:
                    yield return StartCoroutine(EstadoBoss3());
                    break;

            }
        }
    }

    IEnumerator EstadoBoss1()
    {
        float distAPlayer = Vector3.Distance(playerScript.transform.position, transform.position);
        if (distAPlayer < radioAtaque)
        {

            agent.destination = playerScript.transform.position;

            if (distAPlayer < rangoAtaque1 && coolDownTimer == 0)
            {
                anim.SetBool("AtacarEsfera", true);
                Instantiate(esferaMagica, spawnEsfera.transform.position, spawnEsfera.transform.rotation);
                coolDownTimer = coolDown;
            }



        }

        if (vidaBoss <= 350f)
        {
            estadoActual = EstadosBoss.Estado2;
        }

        yield return new WaitForSeconds(1f);
    }

    IEnumerator EstadoBoss2()
    {
        
        float distAPlayer = Vector3.Distance(playerScript.transform.position, transform.position);
        if (distAPlayer < rangoAtaque2 && coolDownTimer == 0)
        {
            agent.destination = playerScript.transform.position;
            anim.SetBool("AtacarEsfera", false);
            anim.SetBool("AtacarMelee", true);                       
            coolDown = coolDownTimer;
        }

        if (vidaBoss <= 200)
        {
            estadoActual = EstadosBoss.Estado3;
            
        }

        yield return new WaitForSeconds(1f);
    }

    IEnumerator EstadoBoss3()
    {
        
        float distAPlayer = Vector3.Distance(playerScript.transform.position, transform.position);
    
        agent.destination = playerScript.transform.position;
       
    
        if (distAPlayer < rangoAtaque2 && coolDownTimer == 0)   
        {
            coolDown = 0.5f;
            anim.SetBool("AtacarEsfera", true);
            Instantiate(esferaMagica, spawnEsfera.transform.position, spawnEsfera.transform.rotation);
            coolDownTimer = coolDown;
                       
        }
        yield return new WaitForSeconds(1f);
    }

    public void InstanciarCapsula()
    {
        float distAPlayer = Vector3.Distance(playerScript.transform.position, transform.position);
        if (distAPlayer < rangoAtaque2)
        {
            Instantiate(capsula, transformCapsula.transform.position, transformCapsula.transform.rotation);
        }
    } 
    


    void CoolDown()
    {
        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

        if (coolDownTimer < 0)
        {
            coolDownTimer = 0;
        }
    }

    

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, radioAtaque);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, rangoAtaque1);

        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, rangoAtaque2);
    }

}
